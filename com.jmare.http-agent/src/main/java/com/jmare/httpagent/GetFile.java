package com.jmare.httpagent;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import com.stmare.file.PathUtil;
import com.stmare.http.ErrorStatusException;
import com.stmare.http.HUrl;

public class GetFile {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("No URL provided");
            printHelp();
            System.exit(-1);
        }
        if (args.length > 2) {
            System.out.println("Too much parameters");
            printHelp();
            System.exit(-1);
        }

        String url = args[0];
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            System.out.println("URL must start with http[s]://");
            printHelp();
            System.exit(-1);
        }
        String resultFilePath;
        if (args.length == 2) {
            resultFilePath = args[1];
        } else {
            resultFilePath = PathUtil.getLastPartFromPath(url);
            if (resultFilePath.length() > 100 || resultFilePath.matches("[\\*\\&\\?]")) {
                System.out.println("Unable to detect result file name, please pass second argument");
                System.exit(-1);
            }
        }
        byte[] bytes = null;
        try {
            bytes = HUrl.getByteArray(args[0]);
        } catch (ErrorStatusException e) {
            System.out.println("Unable to download file from " + url);
            System.exit(-1);
        }
        try {
            Files.write(new File(resultFilePath).toPath(), bytes);
        } catch (IOException e) {
            System.out.println("Unable to save file to " + resultFilePath);
            System.exit(-1);
        }
    }

    private static void printHelp() {
        System.out.println("Download file from url starting with http[s] to given local path.\nUsage: java -jar getfile.jar <URL> [<result_file_name_path>]");
    }
}
