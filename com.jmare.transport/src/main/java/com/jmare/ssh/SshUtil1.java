package com.jmare.ssh;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.jmare.ssh.exception.SshSessionException;
import com.jmare.ssh.sshclient.GanymedCommandUtil;
import com.jmare.ssh.sshclient.SshCommandUtil;
import com.stmare.exception.ExceptionMessage;
import com.stmare.file.FileUtil;

public class SshUtil1 {

    public static void main(String[] args) {
        String username = null;
        String password = null;
        char[] privateKey = null;
        boolean tolerant = false;
        Integer port = null;
        Options options = getOptions();
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options, args);

            if (cmd.hasOption("h")) {
                printHelp(options);
                return;
            }

            List<String> argList = cmd.getArgList();
            if (argList.size() != 2) {
                printHelp(options);
                System.exit(-1);
            }

            if (cmd.hasOption("l")) {
                username = cmd.getOptionValue("l");
            }

            if (cmd.hasOption("tolerant")) {
                tolerant = true;
            }

            if (cmd.hasOption("pw")) {
                password = cmd.getOptionValue("pw");
            } else {
                password = System.getenv("SSH_PASSWORD");
            }

            if (cmd.hasOption("i")) {
                String privateKeyFile = cmd.getOptionValue("i");
                String fileTextSafe;
                try {
                    fileTextSafe = FileUtil.getFileTextSafe(privateKeyFile, StandardCharsets.US_ASCII.name());
                    privateKey = fileTextSafe.toCharArray();
                } catch (Exception e) {
                    System.out.println("Unable to read file " + privateKeyFile);
                    System.exit(-1);
                }
            } else {
                String privateKeyString = System.getenv("SSH_PRIVATE_KEY");
                if (privateKeyString != null) {
                    privateKey = privateKeyString.toCharArray();
                }
            }

            if (cmd.hasOption("p")) {
                try {
                    port = Integer.parseInt(cmd.getOptionValue("p"));
                } catch (NumberFormatException e) {
                    System.out.println("Invalid port");
                    printHelp(options);
                    System.exit(-1);
                }
            } else {
                port = 22;
            }

            String host = argList.get(0);
            int indexOf = host.indexOf("@");
            if (indexOf != -1) {
                username = host.substring(0, indexOf);
                host = host.substring(indexOf + 1);
            }

            String command = argList.get(1);

            try {
                StringBuilder sb = new StringBuilder();
                GanymedCommandUtil.doCommand(host, port, username, password, privateKey, c -> {
                    try {
                        String doCommand = SshCommandUtil.doCommandReturnString(c, command);
                        if (doCommand != null) {
                            sb.append(doCommand).append(System.getProperty("line.separator"));
                        }
                    } catch (IOException e) {
                        throw new SshSessionException("Error: " + command, e);
                    }
                });
                String result = sb.toString();
                if (result != null) {
                    System.out.println(result.trim());
                }
            } catch (Exception e) {
                if (e instanceof IOException) {
                    if (e.getCause() != null && e.getCause().getMessage() != null && e.getCause().getMessage().contains("'-----BEGIN...' missing")) {
                        System.out.println("Probably BEGIN OPENSSH key used which is not supported");
                    }
                }
                if (tolerant) {
                    System.out.println("Command " + command + " not successful but returning exit code 0");
                    return;
                }
                System.out.println(ExceptionMessage.getCombinedMessage(e));
                System.exit(-1);
            }
        } catch (ParseException e) {
            printHelp(options);
            System.exit(-1);
        }
    }

    private static void printHelp(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("java -jar sshutil.jar <OPTIONS> <[username@]HOST> <COMMAND>", options);
    }

    public static Options getOptions() {
        Options options = new Options();

        options.addOption("l", true, "Username");
        options.addOption("pw", true, "Passphrase for private key or password for username");
        options.addOption("i", true, "A file from which the identity key (private key) for public key authentication is read.");
        options.addOption("p", true, "Port to connect to on the remote host. ");
        options.addOption("tolerant", false, "When used the command exit code <> 0 will be propagated as 0");
        options.addOption("h", false, "This help");

        return options;
    }
}
