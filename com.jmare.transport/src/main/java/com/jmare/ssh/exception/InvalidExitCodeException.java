package com.jmare.ssh.exception;

public class InvalidExitCodeException extends RuntimeException {
    private static final long serialVersionUID = -7197035608717759225L;
    
    private Integer exitCode;

    public InvalidExitCodeException(Integer exitCode) {
        super();
        this.exitCode = exitCode;
    }
    
    public InvalidExitCodeException(Integer exitCode, String message) {
        super(message);
        this.exitCode = exitCode;
    }
    
    public Integer getExitCode() {
        return exitCode;
    }
}
