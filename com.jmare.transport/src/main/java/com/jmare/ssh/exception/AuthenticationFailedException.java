package com.jmare.ssh.exception;

public class AuthenticationFailedException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = -4545795946305225901L;

    public AuthenticationFailedException(String message, Throwable cause) {
        super(message, cause);
        
    }

    public AuthenticationFailedException(String message) {
        super(message);
        
    }
}
