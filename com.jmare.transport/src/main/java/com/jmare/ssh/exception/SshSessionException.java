package com.jmare.ssh.exception;

public class SshSessionException extends RuntimeException {
    /**
     * 
     */
    private static final long serialVersionUID = -4545795946305225901L;

    public SshSessionException(String message, Throwable cause) {
        super(message, cause);
    }

    public SshSessionException(Throwable cause) {
        super(cause);
    }

    public SshSessionException(String message) {
        super(message);
    }
}
