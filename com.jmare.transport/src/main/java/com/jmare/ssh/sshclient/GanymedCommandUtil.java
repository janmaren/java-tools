package com.jmare.ssh.sshclient;

import java.io.IOException;
import java.util.function.Consumer;

import com.jmare.ssh.sshclient.SshConnectionFactory.ConnectionType;

import ch.ethz.ssh2.Connection;

public class GanymedCommandUtil {
    public static void doCommand(String hostname, int port, String username, String password, char[] privateKey, Consumer<Connection> command) throws IOException
             {
        SshConnectionFactory sshConnectionFactory = new SshConnectionFactory();
        sshConnectionFactory.setConnectionType(ConnectionType.BY_AUTO);
        sshConnectionFactory.setHostname(hostname);
        sshConnectionFactory.setPort(port);
        sshConnectionFactory.setUsername(username);
        sshConnectionFactory.setPassword(password);
        sshConnectionFactory.setPrivateKey(privateKey);
        sshConnectionFactory.setPrivateKeyPassword(password);
        Connection connection = sshConnectionFactory.getConnection();
        try {
            command.accept(connection);
        } finally {
            connection.close();
        }
    }
}
