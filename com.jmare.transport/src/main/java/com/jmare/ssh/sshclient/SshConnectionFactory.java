package com.jmare.ssh.sshclient;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import com.stmare.file.FileUtil;

import ch.ethz.ssh2.Connection;

public class SshConnectionFactory {
    /**
     * Mandatory host name
     */
    private String hostname;

    private int port = 22;

    /**
     * Mandatory user name
     */
    private String username;

    /**
     * Must be populated only when {@link ConnectionType#BY_CREDENTIONALS}
     */
    private String password;

    /**
     * Must be populated only when {@link ConnectionType#BY_CERTIFICATE} and
     * when certificate is encrypted
     */
    private String privateKeyPassword;

    private ConnectionType connectionType = ConnectionType.BY_AUTO;

    private Connection connection;

    /**
     * Must be populated only when {@link ConnectionType#BY_CERTIFICATE}
     */
    private char[] privateKey;

    public Connection getConnection() throws IOException {
        if (connection == null) {
            connection = new Connection(hostname, port);
            connection.connect();

            if (connectionType == ConnectionType.BY_CERTIFICATE) {
                boolean isAuthenticated = connection.authenticateWithPublicKey(username, privateKey, privateKeyPassword);
                if (isAuthenticated == false) {
                    connection.close();
                    throw new IOException("Authentication failed.");
                }
            } else if (connectionType == ConnectionType.BY_CREDENTIONALS) {
                boolean isAuthenticated = connection.authenticateWithPassword(username, password);
                if (isAuthenticated == false) {
                    connection.close();
                    throw new IOException("Authentication failed.");
                }
            } else if (connectionType == ConnectionType.BY_AUTO) {
                boolean authenticated = false;
                if (privateKey != null) {
                    authenticated = connection.authenticateWithPublicKey(username, privateKey, privateKeyPassword);
                }
                if (!authenticated) {
                    String rsa = LocationUtil.getRsa();
                    if (new File(rsa).isFile()) {
                        char[] privateKey = FileUtil.getFileTextSafe(rsa, StandardCharsets.US_ASCII.name()).toCharArray();
                        try {
                            authenticated = connection.authenticateWithPublicKey(username, privateKey, privateKeyPassword);
                        } catch (IOException e) {
                            // no handling
                        }
                    }
                }
                if (!authenticated) {
                    String dsa = LocationUtil.getDsa();
                    if (new File(dsa).isFile()) {
                        char[] privateKey = FileUtil.getFileTextSafe(dsa, StandardCharsets.US_ASCII.name()).toCharArray();
                        try {
                            authenticated = connection.authenticateWithPublicKey(username, privateKey, privateKeyPassword);
                        } catch (IOException e) {
                            // no handling
                        }
                    }
                }
                if (!authenticated && password != null) {
                    authenticated = connection.authenticateWithPassword(username, password);
                }
                if (!authenticated) {
                    connection.close();
                    throw new IOException("Authentication failed.");
                }
            }
        } else {
            throw new RuntimeException("Getting connection for second time");
        }

        return connection;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPrivateKeyPassword() {
        return privateKeyPassword;
    }

    public void setPrivateKeyPassword(String privateKeyPassword) {
        this.privateKeyPassword = privateKeyPassword;
    }

    public ConnectionType getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(ConnectionType connectionType) {
        this.connectionType = connectionType;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public enum ConnectionType {
        BY_CERTIFICATE, BY_CREDENTIONALS, BY_AUTO
    }

    @Override
    public String toString() {
        return username + "@" + hostname + ":" + port;
    }

    public void setPrivateKey(char[] privateKey) {
        this.privateKey = privateKey;
    }

    public char[] getPrivateKey() {
        return privateKey;
    }


}
