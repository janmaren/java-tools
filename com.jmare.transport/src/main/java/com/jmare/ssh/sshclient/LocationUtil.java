package com.jmare.ssh.sshclient;

import java.io.File;

public class LocationUtil {
    private final static String USER_HOME = System.getProperty("user.home");

    public static String getRsa() {
        return USER_HOME + File.separator + ".ssh" + File.separator + "id_rsa";
    }

    public static String getDsa() {
        return USER_HOME + File.separator + ".ssh" + File.separator + "id_dsa";
    }
}
