package com.jmare.ssh.sshclient;

import java.io.IOException;
import java.io.InputStream;

import com.jmare.ssh.exception.InvalidExitCodeException;
import com.stmare.stream.InputStreamReadUtil;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;
public class SshCommandUtil {
    public static void executeCommand(Connection connection, String command) throws IOException {
        Session session = connection.openSession();
        try {
            session.execCommand(command);

            Integer exitStatus = session.getExitStatus();
            if (exitStatus != null && exitStatus.intValue() != 0) {
                throw new InvalidExitCodeException(exitStatus, "Error: " + command);
            }
        } finally {
            session.close();
        }
    }

    public static String doCommandReturnString(Connection connection, String command, String encoding, int activityTimeout) throws IOException {
        Session session = connection.openSession();
        try {
            session.execCommand(command);

            Integer exitStatus = session.getExitStatus();
            if (exitStatus != null && exitStatus.intValue() != 0) {
                throw new InvalidExitCodeException(exitStatus, "Error: " + command);
            }

            InputStream stdout = new StreamGobbler(session.getStdout());
            String content = InputStreamReadUtil.readTextWithTimeout(stdout, activityTimeout, "UTF-8");
            if (content.length() == 0) {
                InputStream stderr = new StreamGobbler(session.getStderr());
                String errText = InputStreamReadUtil.readTextWithTimeout(stderr, 500, "UTF-8");
                if (errText.length() > 0) {
                    throw new InvalidExitCodeException(-1, "Error in command " + command + ": " + errText);
                }
            }

            exitStatus = session.getExitStatus();
            if (exitStatus != null && exitStatus.intValue() != 0) {
                throw new InvalidExitCodeException(exitStatus, "Error: " + command);
            }

            return content;
        } finally {
            session.close();
        }
    }

    public static String doCommandReturnString(Connection connection, String command) throws IOException {
        return doCommandReturnString(connection, command, "US-ASCII", 3000);
    }
}
