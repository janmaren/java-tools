package com.jmare.ssh.sshjclient;

import java.io.IOException;

import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.transport.verification.PromiscuousVerifier;


public class SshjConnectionFactory {
    /**
     * Mandatory host name
     */
    private String hostname;

    private int port = 22;

    /**
     * Mandatory user name
     */
    private String username;

    /**
     * Must be populated only when {@link ConnectionType#BY_CREDENTIONALS}
     */
    private String password;

    /**
     * Must be populated only when {@link ConnectionType#BY_CERTIFICATE} and
     * when certificate is encrypted
     */
    private String privateKeyPassword;

    private ConnectionType connectionType = ConnectionType.BY_AUTO;

    private SSHClient authenticatedSshClient;

    /**
     * Must be populated only when {@link ConnectionType#BY_CERTIFICATE}
     */
    private String privateKey;

    public SSHClient getAuthenticatedSshClient() throws IOException {
        if (authenticatedSshClient == null) {
            if (connectionType == ConnectionType.BY_CERTIFICATE) {
                try {
                    connect(privateKey);
                } catch (Exception e) {
                    throw new IOException("Unable to authenticate", e);
                }
            } else if (connectionType == ConnectionType.BY_CREDENTIONALS) {
                try {
                    connect();
                } catch (Exception e) {
                    throw new IOException("Unable to authenticate", e);
                }
            } else if (connectionType == ConnectionType.BY_AUTO) {
                try {
                    connectAuto();
                    return authenticatedSshClient;
                } catch (Exception e1) {
                    throw new IOException("Authentication failed.");
                }
            }
        } else {
            throw new RuntimeException("Getting connection for second time");
        }

        return authenticatedSshClient;
    }

    private void connect(String privateKey) throws IOException {
        authenticatedSshClient = new SSHClient();
        authenticatedSshClient.addHostKeyVerifier(new PromiscuousVerifier());
        authenticatedSshClient.connect(hostname, port);
        authenticatedSshClient.authPublickey(username, privateKey);
    }

    private void connect() throws IOException {
        authenticatedSshClient = new SSHClient();
        authenticatedSshClient.addHostKeyVerifier(new PromiscuousVerifier());
        authenticatedSshClient.connect(hostname, port);
        authenticatedSshClient.authPassword(username, password);
    }

    private void connectAuto() throws IOException {
        authenticatedSshClient = new SSHClient();
        authenticatedSshClient.addHostKeyVerifier(new PromiscuousVerifier());
        authenticatedSshClient.connect(hostname, port);
        if (privateKey != null) {
            try {
                authenticatedSshClient.authPublickey(username, privateKey);
                return;
            } catch (Exception e) {
            }
        }
        try {
            authenticatedSshClient.authPublickey(username);
        } catch (Exception e) {
            authenticatedSshClient.authPassword(username, password);
        }
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPrivateKeyPassword() {
        return privateKeyPassword;
    }

    public void setPrivateKeyPassword(String privateKeyPassword) {
        this.privateKeyPassword = privateKeyPassword;
    }

    public ConnectionType getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(ConnectionType connectionType) {
        this.connectionType = connectionType;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public enum ConnectionType {
        BY_CERTIFICATE, BY_CREDENTIONALS, BY_AUTO
    }

    @Override
    public String toString() {
        return username + "@" + hostname + ":" + port;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

}
