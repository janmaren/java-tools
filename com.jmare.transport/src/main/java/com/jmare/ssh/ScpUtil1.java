package com.jmare.ssh;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.jmare.ssh.exception.SshSessionException;
import com.jmare.ssh.sshclient.GanymedCommandUtil;
import com.stmare.exception.ExceptionMessage;
import com.stmare.file.FileUtil;
import com.stmare.file.PathUtil;

import ch.ethz.ssh2.SCPClient;
import ch.ethz.ssh2.SCPInputStream;
import ch.ethz.ssh2.SCPOutputStream;

public class ScpUtil1 {

    public static void main(String[] args) {
        String chmodNumber = "0644";
        String username = null;
        String password = null;
        char[] privateKey = null;
        Integer port = null;
        boolean tolerant = false;
        Options options = getOptions();
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options, args);

            if (cmd.hasOption("h")) {
                printHelp(options);
                return;
            }

            List<String> argList = cmd.getArgList();
            if (argList.size() != 2) {
                printHelp(options);
                System.exit(-1);
            }

            if (cmd.hasOption("l")) {
                username = cmd.getOptionValue("l");
            }

            if (cmd.hasOption("pw")) {
                password = cmd.getOptionValue("pw");
            } else {
                password = System.getenv("SSH_PASSWORD");
            }

            if (cmd.hasOption("tolerant")) {
                tolerant = true;
            }

            if (cmd.hasOption("i")) {
                String privateKeyFile = cmd.getOptionValue("i");
                String fileTextSafe;
                try {
                    fileTextSafe = FileUtil.getFileTextSafe(privateKeyFile, StandardCharsets.US_ASCII.name());
                    privateKey = fileTextSafe.toCharArray();
                } catch (Exception e) {
                    System.out.println("Unable to read file " + privateKeyFile);
                    System.exit(-1);
                }
            } else {
                String privateKeyString = System.getenv("SSH_PRIVATE_KEY");
                if (privateKeyString != null) {
                    privateKey = privateKeyString.toCharArray();
                }
            }

            if (cmd.hasOption("p")) {
                try {
                    port = Integer.parseInt(cmd.getOptionValue("p"));
                } catch (NumberFormatException e) {
                    System.out.println("Invalid port");
                    printHelp(options);
                    System.exit(-1);
                }
            } else {
                port = 22;
            }

            String from = argList.get(0);
            String to = argList.get(1);

            if (hasHost(from) && hasHost(to)) {
                System.out.println("ERROR: both sides are remote");
                System.exit(-1);
            }
            if (!hasHost(from) && !hasHost(to)) {
                System.out.println("ERROR: both sides are locale");
                System.exit(-1);
            }

            String host = null;
            String fromPath = null;
            String toPath = null;
            boolean outbound = false;
            if (hasHost(to)) {
                host = parseHost(to);
                String usr = parseUsername(to);
                if (usr != null) {
                    username = usr;
                }
                toPath = parsePath(to);
                outbound = true;
                fromPath = from;
            } else {
                host = parseHost(from);
                String usr = parseUsername(from);
                if (usr != null) {
                    username = usr;
                }
                fromPath = parsePath(from);
                toPath = to;
            }
            boolean outb = outbound;
            String frpa = fromPath;
            String topa = toPath;
            try {
                GanymedCommandUtil.doCommand(host, port, username, password, privateKey, c -> {
                    try {
                        SCPClient sCPClient = c.createSCPClient();
                        if (outb) {
                            copyLocalToRemote(sCPClient, frpa, topa, chmodNumber);
                        } else {
                            copyRemoteToLocal(sCPClient, frpa, topa);
                        }
                    } catch (IOException e) {
                        throw new SshSessionException("Problems to copy scp " + from + "->" + to + ". " + (e.getMessage() != null ? e.getMessage() : ""), e);
                    }
                });
            } catch (Exception e) {
                if (e instanceof IOException) {
                    if (e.getCause() != null && e.getCause().getMessage() != null && e.getCause().getMessage().contains("'-----BEGIN...' missing")) {
                        System.out.println("Probably BEGIN OPENSSH key used which is not supported");
                    }
                }
                if (tolerant) {
                    System.out.println("scp " + from + "->" + to + " not successful but returning exit code 0");
                    return;
                }
                System.out.println(ExceptionMessage.getCombinedMessage(e));
                System.exit(-1);
            }
        } catch (ParseException e) {
            System.out.println(ExceptionMessage.getCombinedMessage(e));
            printHelp(options);
            System.exit(-1);
        }
    }

    private static void copyRemoteToLocal(SCPClient sCPClient, String frpa, String topa) throws IOException, FileNotFoundException {
        byte[] buffer = new byte[65536];
        String filename = PathUtil.getLastPartFromPath(frpa);
        try (SCPInputStream scpInputStream = sCPClient.get(frpa); FileOutputStream os = new FileOutputStream(new File(topa, filename))) {
            FileUtil.copyLarge(scpInputStream, os, buffer);
        }
    }

    private static void copyLocalToRemote(SCPClient sCPClient, String frpa, String topa, String chmodNumber)
            throws IOException, FileNotFoundException {
        String remoteFilename = PathUtil.getLastPartFromPath(frpa);
        long length = new File(frpa).length();
        byte[] buffer = new byte[65536];
        try (SCPOutputStream outputStream = sCPClient.put(remoteFilename, length,
                topa, chmodNumber);
                FileInputStream fileInputStream = new FileInputStream(frpa)) {
            FileUtil.copyLarge(fileInputStream, outputStream, buffer);
        }
    }

    private static String parsePath(String str) {
        int indexOf = str.indexOf(":");
        if (indexOf != -1) {
            return str.substring(indexOf + 1);
        }
        return str;
    }

    private static String parseUsername(String str) {
        String host = str;
        int indexOf = str.indexOf(":");
        if (indexOf != -1) {
            host = str.substring(0, indexOf);
        }
        int indexOfA = host.indexOf("@");
        if (indexOfA == -1) {
            return null;
        }
        return host.substring(0, indexOfA);
    }

    private static String parseHost(String str) {
        String host = str;
        int indexOf = str.indexOf(":");
        if (indexOf != -1) {
            host = str.substring(0, indexOf);
        }
        int indexOfA = host.indexOf("@");
        if (indexOfA != -1) {
            host = host.substring(indexOfA + 1);
        }
        return host;
    }

    private static boolean hasHost(String str) {
        int indexOf = str.indexOf(":");
        if (indexOf != -1) {
            return indexOf >= 2; // lenght 1 is dos c:\path
        }
        return false;
    }

    private static void printHelp(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("java -jar scputil.jar <OPTIONS> [<[username@]HOST>:]/from/path/file [<[username@]HOST>:]/to/path/file", options);
    }

    public static Options getOptions() {
        Options options = new Options();

        options.addOption("l", true, "Username");
        options.addOption("pw", true, "Passphrase for private key or password for username");
        options.addOption("i", true, "A file from which the identity key (private key) for public key authentication is read.");
        options.addOption("p", true, "Port to connect to on the remote host. ");
        options.addOption("tolerant", false, "When used the command exit code <> 0 will be propagated as 0");
        options.addOption("h", false, "This help");

        return options;
    }
}
