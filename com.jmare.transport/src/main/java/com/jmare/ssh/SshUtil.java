package com.jmare.ssh;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.jmare.ssh.exception.InvalidExitCodeException;
import com.jmare.ssh.sshjclient.SshjConnectionFactory;
import com.stmare.exception.ExceptionMessage;
import com.stmare.file.FileUtil;
import com.stmare.stream.InputStreamReadUtil;

import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.connection.channel.direct.Session;

public class SshUtil {

    public static void main(String[] args) {
        String username = null;
        String password = null;
        String privateKey = null;
        boolean tolerant = false;
        Integer port = null;
        Integer wholeTimeout = null;
        Integer activityTimeout = null;
        String charset = Charset.defaultCharset().name();
        Options options = getOptions();
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options, args);

            if (cmd.hasOption("h")) {
                printHelp(options);
                return;
            }

            List<String> argList = cmd.getArgList();
            if (argList.size() != 2) {
                printHelp(options);
                System.exit(-1);
            }

            if (cmd.hasOption("l")) {
                username = cmd.getOptionValue("l");
            }

            if (cmd.hasOption("tolerant")) {
                tolerant = true;
            }

            if (cmd.hasOption("ta")) {
                try {
                    activityTimeout = Integer.parseInt(cmd.getOptionValue("ta"));
                } catch (NumberFormatException e) {
                    System.out.println("Invalid activity timeout");
                    printHelp(options);
                    System.exit(-1);
                }
            }

            if (cmd.hasOption("tw")) {
                try {
                    wholeTimeout = Integer.parseInt(cmd.getOptionValue("tw"));
                } catch (NumberFormatException e) {
                    System.out.println("Invalid whole timeout");
                    printHelp(options);
                    System.exit(-1);
                }
            }

            if (cmd.hasOption("c")) {
                charset = cmd.getOptionValue("c");
                try {
                    Charset.forName(charset);
                } catch (Exception e) {
                    System.out.println("Invalid charset " + charset);
                    printHelp(options);
                    System.exit(-1);
                }
            }
            
            if (cmd.hasOption("pw")) {
                password = cmd.getOptionValue("pw");
            } else {
                password = System.getenv("SSH_PASSWORD");
            }

            if (cmd.hasOption("i")) {
                privateKey = cmd.getOptionValue("i");
            } else {
                String privateKeyString = System.getenv("SSH_PRIVATE_KEY");
                if (privateKeyString != null) {
                    try {
                        File createTempFile = File.createTempFile("privkey", null);
                        createTempFile.deleteOnExit();
                        try {
                            Files.write(createTempFile.toPath(), privateKeyString.getBytes());
                            privateKey = createTempFile.toString();
                        } catch (IOException e) {
                        }
                    } catch (IOException e) {
                        System.out.println("Unable to create temporary file to store private file");
                    }
                }
            }

            if (cmd.hasOption("p")) {
                try {
                    port = Integer.parseInt(cmd.getOptionValue("p"));
                } catch (NumberFormatException e) {
                    System.out.println("Invalid port");
                    printHelp(options);
                    System.exit(-1);
                }
            } else {
                port = 22;
            }

            String host = argList.get(0);
            int indexOf = host.indexOf("@");
            if (indexOf != -1) {
                username = host.substring(0, indexOf);
                host = host.substring(indexOf + 1);
            }

            String command = argList.get(1);

            SSHClient authenticatedSshClient = null;
            try {
                String result = null;

                SshjConnectionFactory sshConnectionFactory = new SshjConnectionFactory();
                sshConnectionFactory.setConnectionType(SshjConnectionFactory.ConnectionType.BY_AUTO);
                sshConnectionFactory.setHostname(host);
                sshConnectionFactory.setPort(port);
                sshConnectionFactory.setUsername(username);
                sshConnectionFactory.setPassword(password);
                sshConnectionFactory.setPrivateKey(privateKey);
                sshConnectionFactory.setPrivateKeyPassword(password);
                authenticatedSshClient = sshConnectionFactory.getAuthenticatedSshClient();

                try (Session session = authenticatedSshClient.startSession()) {
                    Session.Command sesCommand = session.exec(command);
                    InputStream inputStream = sesCommand.getInputStream();
                    if (wholeTimeout != null) {
                        byte[] bytes = readBytesWithWholeTimeout(inputStream, wholeTimeout);
                        result = new String(bytes, charset);
                    } else if (activityTimeout != null) {
                        result = InputStreamReadUtil.readTextWithTimeout(inputStream, activityTimeout, charset);
                    } else {
                        result = FileUtil.getFileText(inputStream, charset);
                    }
                    inputStream.close();
                    session.join(1, TimeUnit.SECONDS);
                    Integer exitStatus = sesCommand.getExitStatus();
                    if (exitStatus == null || exitStatus != 0) {
                        throw new InvalidExitCodeException(exitStatus, result);
                    }
                }

                if (result != null) {
                    System.out.println(result.trim());
                }
            } catch (InvalidExitCodeException e) {
                if (tolerant) {
                    System.out.println("Command '" + command + "' returned exit code " + e.getExitCode() + " but returning exit code 0 instead (tolerant set)");
                    return;
                }
                System.out.println("Command '" + command + "' returned exit code " + e.getExitCode() + (e.getMessage() != null && !"".equals(e.getMessage().trim()) ? " and message: " + e.getMessage() : ""));
                System.exit(-1);
            } catch (Exception e) {
                if (tolerant) {
                    System.out.println("Command '" + command + "' not successful but returning exit code 0");
                    return;
                }
                System.out.println(ExceptionMessage.getCombinedMessage(e) + (e.getCause() != null && e.getCause().getMessage() != null ? ". " + e.getCause().getMessage() : ""));
                System.exit(-1);
            } finally {
                if (authenticatedSshClient != null) {
                    try {
                        authenticatedSshClient.close();
                    } catch (IOException e) {
                    }
                }
            }
        } catch (ParseException e) {
            printHelp(options);
            System.exit(-1);
        }
    }

    private static void printHelp(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("java -jar sshutil.jar <OPTIONS> <[username@]HOST> <COMMAND>", options);
    }

    public static Options getOptions() {
        Options options = new Options();

        options.addOption("l", true, "Username");
        options.addOption("pw", true, "Passphrase for private key or password for username");
        options.addOption("i", true, "A file from which the identity key (private key) for public key authentication is read.");
        options.addOption("p", true, "Port to connect to on the remote host. ");
        options.addOption("tolerant", false, "When used the command exit code <> 0 will be propagated as 0");
        options.addOption("ta", true, "Activity timeout [ms] - input stream is cut when inactivity longer then this timeout. Note - the result exit code is not guaranteed and may be even undefined");
        options.addOption("tw", true, "Whole timeout [ms] - input stream is cut when communication is longer than this timeout. Note - the result exit code is not guaranteed and may be even undefined");
        options.addOption("c", true, "Charset. When not populated the default charset of you system will be used.");
        options.addOption("h", false, "This help");

        return options;
    }
    
    public static byte[] readBytesWithWholeTimeout(InputStream inputStream, int activityTimeout) throws IOException {
        byte[] bytes = new byte[8192];

        int count;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while ((count = InputStreamReadUtil.readBytesWithWholeTimeout(inputStream, bytes, activityTimeout)) == bytes.length) {
            byteArrayOutputStream.write(bytes, 0, count);
        }
        byteArrayOutputStream.close();

        return byteArrayOutputStream.toByteArray();
    }    
}
