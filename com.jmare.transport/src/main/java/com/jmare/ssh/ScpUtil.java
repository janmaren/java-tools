package com.jmare.ssh;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.jmare.ssh.sshjclient.SshjConnectionFactory;
import com.stmare.exception.ExceptionMessage;

import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.xfer.scp.SCPFileTransfer;

public class ScpUtil {

    public static void main(String[] args) {
        String username = null;
        String password = null;
        String privateKey = null;
        boolean tolerant = false;
        Integer port = null;
        Options options = getOptions();
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options, args);

            if (cmd.hasOption("h")) {
                printHelp(options);
                return;
            }

            List<String> argList = cmd.getArgList();
            if (argList.size() != 2) {
                printHelp(options);
                System.exit(-1);
            }

            if (cmd.hasOption("l")) {
                username = cmd.getOptionValue("l");
            }

            if (cmd.hasOption("tolerant")) {
                tolerant = true;
            }

            if (cmd.hasOption("pw")) {
                password = cmd.getOptionValue("pw");
            } else {
                password = System.getenv("SSH_PASSWORD");
            }

            if (cmd.hasOption("i")) {
                privateKey = cmd.getOptionValue("i");
            } else {
                String privateKeyString = System.getenv("SSH_PRIVATE_KEY");
                if (privateKeyString != null) {
                    try {
                        File createTempFile = File.createTempFile("privkey", null);
                        createTempFile.deleteOnExit();
                        try {
                            Files.write(createTempFile.toPath(), privateKeyString.getBytes());
                            privateKey = createTempFile.toString();
                        } catch (IOException e) {
                        }
                    } catch (IOException e) {
                        System.out.println("Unable to create temporary file to store private file");
                    }
                }
            }

            if (cmd.hasOption("p")) {
                try {
                    port = Integer.parseInt(cmd.getOptionValue("p"));
                } catch (NumberFormatException e) {
                    System.out.println("Invalid port");
                    printHelp(options);
                    System.exit(-1);
                }
            } else {
                port = 22;
            }

            String from = argList.get(0);
            String to = argList.get(1);

            if (hasHost(from) && hasHost(to)) {
                System.out.println("ERROR: both sides are remote");
                System.exit(-1);
            }
            if (!hasHost(from) && !hasHost(to)) {
                System.out.println("ERROR: both sides are locale");
                System.exit(-1);
            }

            String host = null;
            String fromPath = null;
            String toPath = null;
            boolean outbound = false;
            if (hasHost(to)) {
                host = parseHost(to);
                String usr = parseUsername(to);
                if (usr != null) {
                    username = usr;
                }
                toPath = parsePath(to);
                if ("".equals(toPath)) {
                    toPath = ".";
                }
                outbound = true;
                fromPath = from;
            } else {
                host = parseHost(from);
                String usr = parseUsername(from);
                if (usr != null) {
                    username = usr;
                }
                fromPath = parsePath(from);
                toPath = to;
            }

            SSHClient authenticatedSshClient = null;
            try {
                SshjConnectionFactory sshConnectionFactory = new SshjConnectionFactory();
                sshConnectionFactory.setConnectionType(SshjConnectionFactory.ConnectionType.BY_AUTO);
                sshConnectionFactory.setHostname(host);
                sshConnectionFactory.setPort(port);
                sshConnectionFactory.setUsername(username);
                sshConnectionFactory.setPassword(password);
                sshConnectionFactory.setPrivateKey(privateKey);
                sshConnectionFactory.setPrivateKeyPassword(password);
                authenticatedSshClient = sshConnectionFactory.getAuthenticatedSshClient();

                SCPFileTransfer scpFileTransfer = authenticatedSshClient.newSCPFileTransfer().bandwidthLimit(16000);
                if (outbound) {
                    scpFileTransfer.upload(fromPath, toPath);
                } else {
                    scpFileTransfer.download(fromPath, toPath);
                }
            } catch (Exception e) {
                if (tolerant) {
                    System.out.println("scp " + from + "->" + to + " not successful but returning exit code 0" + (e.getMessage() != null && !"".equals(e.getMessage().trim()) ? ". Message: " + e.getMessage() : ""));
                    return;
                }
                System.out.println(ExceptionMessage.getCombinedMessage(e) + (e.getMessage() != null && !"".equals(e.getMessage().trim()) ? ". Message: " + e.getMessage() : ""));
                System.exit(-1);
            } finally {
                if (authenticatedSshClient != null) {
                    try {
                        authenticatedSshClient.close();
                    } catch (IOException e) {
                    }
                }
            }
        } catch (ParseException e) {
            printHelp(options);
            System.exit(-1);
        }
    }

    private static void printHelp(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.setSyntaxPrefix("Copy file or directory (recursivelly) to other location using scp protocol. One of the locations is a remote host\n");
        String footer = "Examples:\njava -jar scputil.jar root@10.0.0.120:/home/john/fil1.txt /home/jack/f.txt\njava -jar scputil.jar root@10.0.0.120:/home/john/file1.txt /home/jack/\njava -jar scputil.jar /home/jack/fil1.txt root@10.0.0.120:/home/john\njava -jar scputil.jar -l root /home/jack/fil1.txt 10.0.0.120:/home/john\njava -jar scputil.jar -i ./id_rsa /tmp/fil1.txt root@10.0.0.120:/tmp";
        formatter.printHelp("java -jar scputil.jar <OPTIONS> <[username@]HOST>:/from/path/file /to/path/[file]\njava -jar scputil.jar <OPTIONS> /from/path/[file] <[username@]HOST>:/to/path/[file]", "", options,  footer);
    }

    public static Options getOptions() {
        Options options = new Options();

        options.addOption("l", true, "Username");
        options.addOption("pw", true, "Passphrase for private key or password for username");
        options.addOption("i", true, "A file from which the identity key (private key) for public key authentication is read.");
        options.addOption("p", true, "Port to connect to on the remote host. ");
        options.addOption("tolerant", false, "When used the command exit code <> 0 will be propagated as 0");
        options.addOption("h", false, "This help");

        return options;
    }

    private static String parsePath(String str) {
        int indexOf = str.indexOf(":");
        if (indexOf != -1) {
            return str.substring(indexOf + 1);
        }
        return str;
    }

    private static String parseUsername(String str) {
        String host = str;
        int indexOf = str.indexOf(":");
        if (indexOf != -1) {
            host = str.substring(0, indexOf);
        }
        int indexOfA = host.indexOf("@");
        if (indexOfA == -1) {
            return null;
        }
        return host.substring(0, indexOfA);
    }

    private static String parseHost(String str) {
        String host = str;
        int indexOf = str.indexOf(":");
        if (indexOf != -1) {
            host = str.substring(0, indexOf);
        }
        int indexOfA = host.indexOf("@");
        if (indexOfA != -1) {
            host = host.substring(indexOfA + 1);
        }
        return host;
    }
    private static boolean hasHost(String str) {
        int indexOf = str.indexOf(":");
        if (indexOf != -1) {
            return indexOf >= 2; // lenght 1 is dos c:\path
        }
        return false;
    }
}
